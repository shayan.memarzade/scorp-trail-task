import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    lang: 'en',
    status: '',
    user: {}
  },
  mutations: {
    auth_success (state, user) {
      state.status = 'success'
      state.user = user
    },
    logout (state) {
      state.status = ''
      state.user = {}
    },
    changeLang (state, lang) {
      state.lang = lang
    }
  },
  actions: {
    login ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_success', user)
        resolve(user)
      })
    },
    getCurrentUser () {
      return this.state.user
    },
    logout ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('logout')
        resolve()
      })
    },
    changeLang ({ commit }, lang) {
      return new Promise((resolve, reject) => {
        commit('changeLang', lang)
        resolve()
      })
    }
  },
  modules: {
  },
  getters: {
    user: state => state.user,
    isLoggedIn: state => state.status,
    lang: state => state.lang
  }
})
